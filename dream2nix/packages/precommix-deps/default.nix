{
  dream2nix,
  config,
  lib,
  self,
  ...
}: let
  topConfig = config;
in {
  imports = [dream2nix.modules.dream2nix.pip];

  name = "precommix-deps";
  version = "0.1.0";

  deps = {nixpkgs, ...}: {
    inherit (nixpkgs) pandoc tree;
  };

  pip = {
    flattenDependencies = true;
    requirementsList = [
      "copier-template-tester"
      "git+https://github.com/OCA/maintainer-tools"
      "pre-commit-po-hooks"
      "pylint-odoo"
    ];
    drvs.oca-maintainers-tools = {config, ...}: {
      buildPythonPackage = {
        pyproject = true;
        build-system = [config.deps.python3.pkgs.hatch-vcs];
        dependencies = [topConfig.deps.pandoc];
      };

      mkDerivation = {
        nativeCheckInputs = [topConfig.deps.tree];
        preBuild = ''
          echo 'global-include **' > MANIFEST.in
        '';

        doCheck = true;
        doInstallCheck = true;
        installCheckPhase = ''
          (
            set -x

            # Create a sample module with readme to test the hooks
            mkdir -p module/readme
            echo '{"version": "17.0.1.0.0"}' > module/__manifest__.py
            echo DESCRIPTION > module/readme/DESCRIPTION.rst

            # Generate the readme
            $out/bin/oca-gen-addon-readme --repo-name repo --branch 17.0 --addon-dir module --convert-fragments-to-markdown
            $out/bin/oca-gen-addon-readme --repo-name repo --branch 17.0 --addon-dir module

            # Check its operation
            tree module
            test ! -f module/readme/DESCRIPTION.rst
            test -f module/readme/DESCRIPTION.md
            test -f module/README.rst
            test -f module/static/description/index.html
          )
        '';
      };
    };
  };
}
