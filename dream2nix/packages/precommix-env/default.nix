{
  config,
  dream2nix,
  lib,
  ...
}: let
  toNormalDrv = module: module.public;
  mapToNormalDrvs = builtins.mapAttrs (_: toNormalDrv);
in rec {
  imports = [
    dream2nix.modules.dream2nix.package-func
  ];

  name = "precommix-env";

  deps = {
    nixpkgs,
    own,
    ...
  }: {
    inherit
      (nixpkgs)
      alejandra
      ansible-lint
      blacken-docs
      buildEnv
      commitizen
      pre-commit
      ruff
      taplo
      opentofu
      tfsec
      ;
    inherit
      (nixpkgs.python311Packages)
      pre-commit-hooks
      restructuredtext_lint
      ;

    postBuildPython = nixpkgs.python3.withPackages (ps: [ps.pyyaml]);

    # Custom hooks
    nothing-added = nixpkgs.writeScriptBin "nothing-added" ''
      #!${nixpkgs.stdenv.shell}
      ! ${nixpkgs.git}/bin/git status --porcelain=v2 | ${nixpkgs.gnugrep}/bin/grep '^?'
    '';

    # Own packages
    inherit
      (own)
      eslint
      prettier
      precommix-deps
      ;
  };

  package-func = let
    hooksYaml = builtins.readFile ../../../.pre-commit-hooks.yaml;
  in {
    outputs = ["out"];
    func = config.deps.buildEnv;
    args = {
      inherit name;
      paths = with config.deps; [
        alejandra
        ansible-lint
        blacken-docs
        commitizen
        eslint
        nothing-added
        opentofu
        pre-commit
        pre-commit-hooks
        precommix-deps.pyEnv
        prettier
        restructuredtext_lint
        ruff
        taplo
        tfsec
      ];
      pathsToLink = ["/bin" config.deps.python3.sitePackages];
      nativeBuildInputs = [config.deps.postBuildPython];
      postBuild = ''
        python ${./post_build.py} ${lib.escapeShellArg hooksYaml}
      '';
    };
  };
}
